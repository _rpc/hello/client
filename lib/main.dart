import 'package:client/src/generated/chat_service/chat_service.pbgrpc.dart';
import 'package:client/src/generated/hello_service/hello_service.pbgrpc.dart';
import 'package:client/src/screens/chat_screen/chat_screen.dart';
import 'package:flutter/material.dart';
import 'package:grpc/grpc.dart';
import 'package:provider/provider.dart';

main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late ClientChannel channel;
  late HelloServiceClient helloServiceClient;
  late ChatServiceClient chatServiceClient;
  initialize() async {
    channel = ClientChannel(
      '192.168.43.48',
      port: 8080,
      options: const ChannelOptions(
        credentials: ChannelCredentials.insecure(),
      ),
    );
    helloServiceClient = HelloServiceClient(channel);
    chatServiceClient = ChatServiceClient(channel);
  }

  @override
  void initState() {
    super.initState();
    initialize();
  }

  @override
  dispose() {
    channel.shutdown();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MultiProvider(
        providers: [
          Provider<HelloServiceClient>.value(
            value: helloServiceClient,
          ),
          Provider<ChatServiceClient>.value(
            value: chatServiceClient,
          )
        ],
        child: ChatScreen(),
      ),
    );
  }
}
