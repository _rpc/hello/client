///
//  Generated code. Do not modify.
//  source: chat_service.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'chat_service.pb.dart' as $0;
export 'chat_service.pb.dart';

class ChatServiceClient extends $grpc.Client {
  static final _$getChats = $grpc.ClientMethod<$0.Void, $0.Chats>(
      '/chatService.ChatService/GetChats',
      ($0.Void value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Chats.fromBuffer(value));
  static final _$addChat = $grpc.ClientMethod<$0.Chat, $0.ChatCreationResponse>(
      '/chatService.ChatService/AddChat',
      ($0.Chat value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.ChatCreationResponse.fromBuffer(value));

  ChatServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseStream<$0.Chats> getChats($0.Void request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(
        _$getChats, $async.Stream.fromIterable([request]),
        options: options);
  }

  $grpc.ResponseFuture<$0.ChatCreationResponse> addChat($0.Chat request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$addChat, request, options: options);
  }
}

abstract class ChatServiceBase extends $grpc.Service {
  $core.String get $name => 'chatService.ChatService';

  ChatServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.Void, $0.Chats>(
        'GetChats',
        getChats_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $0.Void.fromBuffer(value),
        ($0.Chats value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Chat, $0.ChatCreationResponse>(
        'AddChat',
        addChat_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Chat.fromBuffer(value),
        ($0.ChatCreationResponse value) => value.writeToBuffer()));
  }

  $async.Stream<$0.Chats> getChats_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Void> request) async* {
    yield* getChats(call, await request);
  }

  $async.Future<$0.ChatCreationResponse> addChat_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Chat> request) async {
    return addChat(call, await request);
  }

  $async.Stream<$0.Chats> getChats($grpc.ServiceCall call, $0.Void request);
  $async.Future<$0.ChatCreationResponse> addChat(
      $grpc.ServiceCall call, $0.Chat request);
}
