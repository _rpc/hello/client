///
//  Generated code. Do not modify.
//  source: chat_service.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use voidDescriptor instead')
const Void$json = const {
  '1': 'Void',
};

/// Descriptor for `Void`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List voidDescriptor = $convert.base64Decode('CgRWb2lk');
@$core.Deprecated('Use chatDescriptor instead')
const Chat$json = const {
  '1': 'Chat',
  '2': const [
    const {'1': 'sender', '3': 1, '4': 1, '5': 11, '6': '.chatService.Sender', '10': 'sender'},
    const {'1': 'message', '3': 2, '4': 1, '5': 11, '6': '.chatService.Message', '10': 'message'},
  ],
};

/// Descriptor for `Chat`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List chatDescriptor = $convert.base64Decode('CgRDaGF0EisKBnNlbmRlchgBIAEoCzITLmNoYXRTZXJ2aWNlLlNlbmRlclIGc2VuZGVyEi4KB21lc3NhZ2UYAiABKAsyFC5jaGF0U2VydmljZS5NZXNzYWdlUgdtZXNzYWdl');
@$core.Deprecated('Use senderDescriptor instead')
const Sender$json = const {
  '1': 'Sender',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
  ],
};

/// Descriptor for `Sender`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List senderDescriptor = $convert.base64Decode('CgZTZW5kZXISEgoEbmFtZRgBIAEoCVIEbmFtZQ==');
@$core.Deprecated('Use messageDescriptor instead')
const Message$json = const {
  '1': 'Message',
  '2': const [
    const {'1': 'text', '3': 1, '4': 1, '5': 9, '10': 'text'},
  ],
};

/// Descriptor for `Message`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List messageDescriptor = $convert.base64Decode('CgdNZXNzYWdlEhIKBHRleHQYASABKAlSBHRleHQ=');
@$core.Deprecated('Use chatsDescriptor instead')
const Chats$json = const {
  '1': 'Chats',
  '2': const [
    const {'1': 'chat', '3': 1, '4': 3, '5': 11, '6': '.chatService.Chat', '10': 'chat'},
  ],
};

/// Descriptor for `Chats`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List chatsDescriptor = $convert.base64Decode('CgVDaGF0cxIlCgRjaGF0GAEgAygLMhEuY2hhdFNlcnZpY2UuQ2hhdFIEY2hhdA==');
@$core.Deprecated('Use chatCreationResponseDescriptor instead')
const ChatCreationResponse$json = const {
  '1': 'ChatCreationResponse',
  '2': const [
    const {'1': 'success', '3': 1, '4': 1, '5': 8, '10': 'success'},
    const {'1': 'errormessage', '3': 2, '4': 1, '5': 9, '10': 'errormessage'},
  ],
};

/// Descriptor for `ChatCreationResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List chatCreationResponseDescriptor = $convert.base64Decode('ChRDaGF0Q3JlYXRpb25SZXNwb25zZRIYCgdzdWNjZXNzGAEgASgIUgdzdWNjZXNzEiIKDGVycm9ybWVzc2FnZRgCIAEoCVIMZXJyb3JtZXNzYWdl');
