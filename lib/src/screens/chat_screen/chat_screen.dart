import 'package:client/src/generated/chat_service/chat_service.pbgrpc.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ChatScreen extends StatelessWidget {
  final controoler = TextEditingController();
  final scrollController = ScrollController();
  @override
  Widget build(BuildContext context) {
    final c = Provider.of<ChatServiceClient>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Real Time chat'),
      ),
      body: Column(
        children: <Widget>[
          Flexible(
            flex: 8,
            child: StreamBuilder<Chats>(
              stream: c.getChats(Void()),
              builder: (_, snapshot) {
                if (snapshot.hasData) {
                  final chats = snapshot.data?.chat.reversed.toList() ?? [];
                  return ListView.builder(
                    shrinkWrap: true,
                    reverse: true,
                    itemCount: chats.length > 5 ? 5 : chats.length,
                    itemBuilder: (_, i) {
                      return Card(
                        shadowColor: Colors.black.withOpacity(0.2),
                        child: ListTile(
                          leading: CircleAvatar(
                            child: Text(chats[i].sender.name[0].toUpperCase()),
                          ),
                          title: Text(chats[i].message.text),
                        ),
                      );
                    },
                  );
                } else if (snapshot.hasError) {
                  print(snapshot.error.toString());
                  return Text(snapshot.error.toString());
                }
                return CircularProgressIndicator();
              },
            ),
          ),
          Flexible(
            flex: 1,
            child: TextField(
              controller: controoler,
              decoration: InputDecoration(
                suffixIcon: IconButton(
                  onPressed: () async {
                    try {
                      final res = await Provider.of<ChatServiceClient>(context,
                              listen: false)
                          .addChat(Chat(
                        sender: Sender(name: 'biswa'),
                        message: Message(text: controoler.text),
                      ));
                      print(res.toString());
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text('Sent'),
                        ),
                      );
                    } on Exception catch (e) {
                      print(e.toString());
                    }
                  },
                  icon: Icon(Icons.send),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
