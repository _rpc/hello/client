import 'package:client/src/generated/hello_service/hello_service.pbgrpc.dart';
import 'package:faker/faker.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class GreetAllAtOnce extends StatefulWidget {
  @override
  _GreetAllAtOnceState createState() => _GreetAllAtOnceState();
}

class _GreetAllAtOnceState extends State<GreetAllAtOnce> {
  List<String> _sent = [];
  Greeting? _recieved;

  greetPeople() async {
    setState(() {
      _sent.clear();
      _recieved = null;
    });
    _recieved = await Provider.of<HelloServiceClient>(context, listen: false)
        .greetAllAtOnce(
      peopleGenerator(5).map(
        (event) {
          if (mounted)
            setState(() {
              _sent.add(event);
            });
          return Greeted()..name = event;
        },
      ).asBroadcastStream()
        ..listen(null).onDone(
          () {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text("Done Sending"),
              ),
            );
          },
        ),
    );
  }

  Stream<String> peopleGenerator([int howMany = 5]) async* {
    for (var _ in List.generate(howMany, (i) => i)) {
      yield await Future.delayed(
        Duration(seconds: 2),
        () => Faker().person.firstName(),
      );
    }
    return;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Greet All At Once')),
      floatingActionButton: FloatingActionButton(
        onPressed: greetPeople,
        child: Icon(Icons.play_arrow),
      ),
      body: DefaultTextStyle(
        style: Theme.of(context).textTheme.headline2!,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Sent"),
            Expanded(
              flex: 1,
              child: ListView.builder(
                itemBuilder: (_, index) => ListTile(
                  leading: Text(
                    _sent[index],
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ),
                itemCount: _sent.length,
              ),
            ),
            Text("Recieved"),
            Expanded(
              flex: 1,
              child: Center(
                child: DefaultTextStyle(
                    style: Theme.of(context).textTheme.bodyText1!,
                    child: Text(_recieved?.message ?? '...')),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
