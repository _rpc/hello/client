import 'package:client/src/generated/hello_service/hello_service.pbgrpc.dart';
import 'package:faker/faker.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class GreetOne extends StatefulWidget {
  @override
  _GreetOneState createState() => _GreetOneState();
}

class _GreetOneState extends State<GreetOne> {
  String _greeted = '...';
  String _greeting = '...';

  greet() async {
    setState(() {
      _greeted = Faker().person.firstName();
    });
    _greeting = (await Provider.of<HelloServiceClient>(context, listen: false)
            .greet(Greeted()..name = _greeted))
        .message;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("GreetOne"),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.send),
        onPressed: greet,
      ),
      body: Center(
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: Container(
                child: Row(
                  children: [
                    Text(
                      'Greeted: ',
                      style: Theme.of(context).textTheme.headline3,
                    ),
                    Text(
                      _greeted,
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                child: Row(
                  children: [
                    Text(
                      'Greeting: ',
                      style: Theme.of(context).textTheme.headline3,
                    ),
                    Text(
                      _greeting,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
