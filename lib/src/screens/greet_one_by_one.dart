import 'dart:async';
import 'package:client/src/generated/hello_service/hello_service.pbgrpc.dart';
import 'package:faker/faker.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class GreetOneByOne extends StatefulWidget {
  @override
  _GreetOneByOneState createState() => _GreetOneByOneState();
}

class _GreetOneByOneState extends State<GreetOneByOne> {
  bool _isStreaming = false;
  String _sending = '...';
  String _recieving = '...';
  Stream<Greeting>? _responseStream;
  Stream<Greeted>? _requestStream;
  late StreamSubscription<Greeted> _requestStreamSubscription;
  late StreamSubscription _responseStreamSubscription;

  switchStream() async {
    _isStreaming = !_isStreaming;

    if (_isStreaming) {
      _requestStream = nameGenerator().asBroadcastStream();
      _requestStreamSubscription = _requestStream!.listen((event) {
        setState(() {
          _sending = event.name;
        });
      });
      _responseStream = Provider.of<HelloServiceClient>(context, listen: false)
          .greetOneByOne(_requestStream!);
      _responseStreamSubscription = _responseStream!.listen((event) {
        setState(() {
          _recieving = event.message;
        });
      });
    } else {
      _requestStreamSubscription.cancel();
      _responseStreamSubscription.cancel();
      _responseStream = null;
      _requestStream = null;
    }
    setState(() {});
  }

  Stream<Greeted> nameGenerator() async* {
    while (true) {
      yield await Future.delayed(Duration(seconds: 2),
          () => Greeted()..name = Faker().person.firstName());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Greet One By One'),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: switchStream,
        child: Icon(!_isStreaming ? Icons.play_arrow : Icons.stop),
      ),
      body: Center(
        child: Column(
          children: [
            Text(
              'Sending...',
              style: Theme.of(context).textTheme.headline2,
            ),
            Expanded(
              flex: 1,
              child: Center(
                child: Text(_sending),
              ),
            ),
            Text(
              'Receiving...',
              style: Theme.of(context).textTheme.headline2,
            ),
            Expanded(
              flex: 1,
              child: Center(
                child: Text(_recieving),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
