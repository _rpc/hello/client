import 'package:client/src/generated/hello_service/hello_service.pbgrpc.dart';
import 'package:flutter/material.dart';
import 'dart:async';

import 'package:provider/provider.dart';

class GreetRandom extends StatefulWidget {
  @override
  _GreetRandomState createState() => _GreetRandomState();
}

class _GreetRandomState extends State<GreetRandom> {
  late StreamSubscription subscription;
  bool isSubscribed = false;

  String _text = '...';

  subscribe() async {
    isSubscribed = true;
    subscription = Provider.of<HelloServiceClient>(context, listen: false)
        .randomGreetings(Void())
        .listen((value) {
      _text = value.message;
      setState(() {});
    })
          ..onDone(() {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text('Subscription done'),
            ));
            setState(() {
              isSubscribed = false;
            });
          });
    setState(() {});
  }

  unsubscribe() async {
    isSubscribed = false;
    subscription.cancel();
    setState(() {});
  }

  @override
  void dispose() {
    super.dispose();
    subscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Random Greetings"),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: isSubscribed ? unsubscribe : subscribe,
        child: Icon(isSubscribed ? Icons.stop : Icons.play_arrow_rounded),
      ),
      body: Center(
        child: Text(_text),
      ),
    );
  }
}
