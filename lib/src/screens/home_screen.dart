import 'package:client/src/screens/greet_all_at_once.dart';
import 'package:client/src/screens/greet_one.dart';
import 'package:client/src/screens/greet_one_by_one.dart';
import 'package:client/src/screens/greet_random.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        children: [
          GreetOne(),
          GreetRandom(),
          GreetAllAtOnce(),
          GreetOneByOne(),
        ],
      ),
    );
  }
}
